Build:

    cd <project root>
    mkdir build
    cd build
    cmake ..
    make

Launch: ./fme <batch path>

Batch example: <project root>/sample.bat